function searchMovie() {
    const movieTitle = document.getElementById('movieTitle').value;
    const apiKey = '30063268';
    const apiUrl = `https://www.omdbapi.com/?t=${movieTitle}&plot=full&apikey=${apiKey}`;

    fetch(apiUrl)
        .then(response => response.json())
        .then(data => {
            const movieInfo = document.getElementById('movieInfo');
            const movieName = document.getElementById('movieName');
            const movieYear = document.getElementById('movieYear');
            const movieActors = document.getElementById('movieActors');
            const moviePlot = document.getElementById('moviePlot');
            const moviePoster = document.getElementById('moviePoster');

            movieName.textContent = data.Title;
            movieYear.textContent = data.Year;
            movieActors.textContent = data.Actors;
            moviePlot.textContent = data.Plot;
            moviePoster.src = data.Poster;

            movieInfo.classList.remove('hidden');
        })
        .catch(error => console.error('Error fetching movie data:', error));
}
